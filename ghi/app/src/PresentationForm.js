import React from 'react'
import { renderMatches } from 'react-router-dom'

export default class PresentationForm extends React.Component {

        state = {
        presenter_name: '',
        presenter_email:'',
        company_name:'',
        title:'',
        synopsis:'',
        conference:'',
        conferences:[],
        }

    handleNameChange = (event) => {
        const value = event.target.value
        this.setState({presenter_name:value})}


    handleEmailChange = (event) => {
        const value = event.target.value
        this.setState({presenter_email:value})}

    handleCompanyChange = (event) => {
        const value = event.target.value
        this.setState({company_name:value})
    }

    handleTitleChange = (event) => {
        const value = event.target.value
        this.setState({title:value})}

    handleSynopsisChange = (event) => {
        const value = event.target.value
        this.setState({synopsis:value})}

    handleConferenceChange = (event) => {
        const value = event.target.value
        this.setState({conference:value})}


    handleFormSubmit = async (event) => {
        event.preventDefault()
        const data = {...this.state}
        delete data.conferences
        console.log(data)

        const presentationUrl = `http://localhost:8000/${data.conference}presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }

        const response = await fetch(presentationUrl, fetchConfig)

        if (response.ok){
            const newPresentation = await response.json()
            console.log(newPresentation)

            const cleared ={
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title:'',
                synopsis:'',
                conference: ''
            }
            this.setState(cleared)
    }
        else{
            console.log("an error has occurred")
        }
}
    async componentDidMount(){
        const url = 'http://localhost:8000/api/conferences/'
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            this.setState({conferences:data.conferences})
        }
    }


render(){
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit = {this.handleFormSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange ={this.handleNameChange} value = {this.state.presenter_name}  placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleEmailChange} value = {this.state.presenter_email} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleCompanyChange} value = {this.state.company_name} placeholder="company_name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleTitleChange} value = {this.state.title} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis" className="form-label">Synopsis</label>
                <textarea onChange ={this.handleSynopsisChange} value = {this.state.synopsis} className="form-control" name= "synopsis" id="synopsis" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange ={this.handleConferenceChange} value = {this.state.conference} required id="conference" name= "conference" className="form-select">
                  <option value="">Choose a conference</option>
                    {this.state.conferences.map(conference => {
                      return (
                        <option key={conference.id} value={conference.href}>
                          {conference.name}
                        </option>
                      )
                    })};
                </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}















    }
